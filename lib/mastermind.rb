require "byebug"

class Code
  attr_reader :pegs

  PEGS = {
    "R" => "Red",
    "G" => "Green",
    "B" => "Blue",
    "Y" => "Yellow",
    "O" => "Orange",
    "P" => "Purple"
  }

  def initialize(pegs)
    @pegs = pegs
    raise "Invalid input of pegs" if !pegs.is_a?(Array)
  end

  def self.parse(str)
    new_code = str.chars.map(&:upcase)
    unless new_code - PEGS.keys == [] && new_code.size == 4
      raise ArgumentError, "Contains invalid color"
    end
    pegs = new_code.map { |peg| PEGS[peg] }
    self.new(pegs)
  end

  def self.random
    pegs = PEGS.values.shuffle[0..3]
    self.new(pegs)
  end

  def [](idx)
    @pegs[idx]
  end

  def exact_matches(code)
    exact_match = 0
    (0..3).each { |idx| exact_match += 1 if (@pegs[idx] == code.pegs[idx]) }
    exact_match
  end

  def near_matches(code)
    near_match = 0
    PEGS.values.each do |color|
      near_match += [@pegs.count(color), code.pegs.count(color)].min
    end
    near_match - exact_matches(code)
  end

  def ==(code)
    return false unless code.is_a?(Code)
    @pegs == code.pegs
  end

end

class Game
  attr_reader :secret_code

  def initialize(code=nil)
    @secret_code = code || Code.random
  end

  def play
    10.times do |turn|
      puts "You have #{10 - turn} chances for getting the number!"
      guess = get_guess
      break if won?(guess)
      display_matches(guess)
    end
    result
  end

  def get_guess
    print "Guess 4 colors (ex. BGRB):  "
    @guess = Code.parse($stdin.gets.chomp)
  end

  def won?(guess)
    @secret_code == guess
  end

  def display_matches(code)
    puts "------Exact matches is: #{@secret_code.exact_matches(code)}"
    puts "------Near matches is: #{@secret_code.near_matches(code)}"
    puts "-----------------------------------------------------------"
  end

  def result
    if @guess == @secret_code
      puts "You win!"
    else
      puts "Sorry, you lost"
    end
    puts "The secret_code was #{@secret_code.pegs}"
  end
end

if __FILE__ == $PROGRAM_NAME
  game = Game.new(Code.parse("brbr"))
  game.play
end
